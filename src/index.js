const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

// room state: 200 = available , 300 = full or inGame , 400 = not found

const playersNbr = 2;
const startGameAfter = 5000; // ms
let playersCount = 0;
let playersReady = 0;
let playersEnded = 0;
let playersList = [];
let inGame = false;
let gameGuess = 0;


app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});

app.get('/getPlayerCount', (req, res) => {
  res.json({
    playersCount,
  });
});

function broadcastPlayersCount() {
  playersList.forEach((player) => {
    io.to(player.socketID).emit('playersCount', playersCount);
  }, this);
}

function broadcastGameStart() {
  playersList.forEach((player) => {
    io.to(player.socketID).emit('gameStart');
  }, this);
}

function broadcastPlayerWinner(username) {
  playersList.forEach((player) => {
    io.to(player.socketID).emit('playerWin', username);
  }, this);
}
function broadcastGameEnded() {
  playersList.forEach((player) => {
    io.to(player.socketID).emit('gameOver');
  }, this);
}

io.on('connection', (socket) => {
  socket.on('joining', (data) => {
    if (!inGame && playersCount < playersNbr) {
      console.log(`${data} has joined the game on the id ${socket.id}`);
      playersCount += 1;
      broadcastPlayersCount();
      playersList.push({
        username: data,
        socketID: socket.id,
      });
      console.log(playersList);
      io.to(socket.id).emit('lobbyInfo', {
        state: 200,
        playersCount,
      });
    } else {
      io.to(socket.id).emit('joinFailed', {
        state: 300,
        // message: 'You can\'t join the game right now',
      });
    }
  });

  socket.on('ready', () => {
    playersReady += 1;
    if (playersReady === playersNbr) {
      broadcastGameStart();
      setTimeout(() => {
        if (playersCount === playersNbr) {
          inGame = true;
          gameGuess = Math.floor(Math.random() * 99) + 1;
          console.log(gameGuess);
        }
      }, startGameAfter);
    }
  });

  socket.on('gameEnded', () => {
    playersEnded += 1;
    if (playersEnded === playersCount) {
      broadcastGameEnded();
      playersCount = 0;
      playersReady = 0;
      playersEnded = 0;
      playersList = [];
      inGame = false;
      gameGuess = 0;
    }
  });

  socket.on('guess', (data) => {
    if (data.guess === gameGuess) {
      broadcastPlayerWinner(data.username);
    } else {
      const distance = Math.abs(data.guess - gameGuess);
      io.to(socket.id).emit('wrongGuess', {
        index: data.guess,
        distance,
      });
    }
  });

  socket.on('disconnect', () => {
    for (let index = 0; index < playersList.length; index += 1) {
      if (playersList[index].socketID === socket.id) {
        console.log(`${playersList[index].username} has left the game`);
        playersCount -= 1;
        if (playersCount === 0) {
          inGame = false;
        }
        broadcastPlayersCount();
        playersList.splice(index, 1);
        playersReady = 0;
      }
    }
  });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});
